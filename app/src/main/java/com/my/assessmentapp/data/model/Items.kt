package com.my.assessmentapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.my.assessmentapp.data.model.Items.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class Items(
    @PrimaryKey(autoGenerate = true)
    val id : Int = 0,
    val item: String,
    val category_id: Int,
    val price: Double,
    val item_image: String
){
    companion object {
        const val TABLE_NAME = "task_items"
    }
}
