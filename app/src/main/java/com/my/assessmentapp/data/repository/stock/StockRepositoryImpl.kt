package com.my.assessmentapp.data.repository.stock

import androidx.lifecycle.LiveData
import com.my.assessmentapp.data.local.dao.BaseDao
import com.my.assessmentapp.data.local.dao.CategoryDao
import com.my.assessmentapp.data.local.dao.ItemDao
import com.my.assessmentapp.data.model.Category
import com.my.assessmentapp.data.model.Items
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class StockRepositoryImpl(val itemDao: ItemDao,val categoryDao: CategoryDao):StockRepository {
    override suspend fun createStock() {
        categoryDao.insert(Category(category = "Burger",category_image = "test"))
        categoryDao.insert(Category(category = "Sandwich",category_image = "test"))
        categoryDao.insert(Category(category = "Wrap",category_image = "test"))
        categoryDao.insert(Category(category = "Fries",category_image = "test"))
        categoryDao.insert(Category(category = "Chicken",category_image = "test"))
        categoryDao.insert(Category(category = "Roles",category_image = "test"))
        categoryDao.insert(Category(category = "Beef",category_image = "test"))
        categoryDao.insert(Category(category = "Ice creams",category_image = "test"))
        categoryDao.insert(Category(category = "Shakes",category_image = "test"))
        itemDao.insert(Items(item = "Burger combo",category_id = 1,price = 2.50,item_image = "hamburger"))
        itemDao.insert(Items(item = "King Burger",category_id = 1,price = 3.50,item_image = "hamburger"))
        itemDao.insert(Items(item = "Beef Burger",category_id = 1,price = 4.50,item_image = "hamburger"))
        itemDao.insert(Items(item = "Kabab Burger",category_id = 1,price = 2.70,item_image = "hamburger"))
        itemDao.insert(Items(item = "Veg Burger",category_id = 1,price = 1.50,item_image = "hamburger"))
        itemDao.insert(Items(item = "Veg Sandwich",category_id = 2,price = 1.50,item_image = "sandwich"))
        itemDao.insert(Items(item = "Beef Sandwich",category_id = 2,price = 2.50,item_image = "sandwich"))
        itemDao.insert(Items(item = "Chicken Sandwich",category_id = 2,price = 3.50,item_image = "sandwich"))
        itemDao.insert(Items(item = "Chicken wrap",category_id = 3,price = 3.50,item_image = "wrap"))
        itemDao.insert(Items(item = "Beef wrap",category_id = 3,price = 3.50,item_image = "wrap"))
        itemDao.insert(Items(item = "Veg wrap",category_id = 3,price = 3.50,item_image = "wrap"))
        itemDao.insert(Items(item = "Egg wrap",category_id = 3,price = 3.50,item_image = "wrap"))
        itemDao.insert(Items(item = "Fries",category_id = 4,price = 0.50,item_image = "fries"))
    }

    override suspend fun selectCategory(): Flow<List<Category>> = flow {
        val result = categoryDao.selectCategory()
        emit(result)
    }.flowOn(Dispatchers.IO)

    override suspend fun selectItem(id: Int): Flow<List<Items>> = flow {
        val result = itemDao.selectItems(id)
        emit(result)
    }.flowOn(Dispatchers.IO)
}