package com.my.assessmentapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.my.assessmentapp.data.model.Category.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class Category(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val category: String,
    val category_image: String
){
    companion object {
        const val TABLE_NAME = "task_category"
    }
}
