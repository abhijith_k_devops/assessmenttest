package com.my.assessmentapp.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.my.assessmentapp.data.model.OrderedItems.Companion.TABLE_NAME

@Entity(
    tableName = TABLE_NAME
)
data class OrderedItems(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val item_id: Int,
    val item_name: String,
    val price: Double,
    val image: String,
    val order_id: Int = 0,
    val item_count: Int,
    val total_price: Double
){
    companion object {
        const val TABLE_NAME = "task_ordered_items"
    }
}

