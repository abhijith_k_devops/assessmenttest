package com.my.assessmentapp.data.repository.stock

import androidx.lifecycle.LiveData
import com.my.assessmentapp.data.model.Category
import com.my.assessmentapp.data.model.Items
import kotlinx.coroutines.flow.Flow

interface StockRepository {
    suspend fun createStock()

    suspend fun selectCategory(): Flow<List<Category>>

    suspend fun selectItem(id : Int): Flow<List<Items>>
}