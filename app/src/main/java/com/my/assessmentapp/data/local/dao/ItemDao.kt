package com.my.assessmentapp.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.my.assessmentapp.data.model.Items

@Dao
interface ItemDao: BaseDao<Items> {
    @Query("SELECT * FROM ${Items.TABLE_NAME} WHERE category_id = :id")
    fun selectItems(id: Int): List<Items>


}