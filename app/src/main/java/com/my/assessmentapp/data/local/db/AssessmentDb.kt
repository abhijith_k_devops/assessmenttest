package com.my.assessmentapp.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.cs.movieapp.data.local.db.DatabaseMigrations
import com.my.assessmentapp.data.local.dao.*
import com.my.assessmentapp.data.model.Category
import com.my.assessmentapp.data.model.Items
import com.my.assessmentapp.data.model.OrderedItems
import com.my.assessmentapp.data.model.Orders
import com.my.assessmentapp.utils.DateConverter

@Database(
    entities = [Items::class,Category::class,Orders::class,OrderedItems::class],
    version = DatabaseMigrations.DB_VERSION
)
abstract class AssessmentDb : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao
    abstract fun orderDao(): OrderDao
    abstract fun itemDao(): ItemDao
    abstract fun orderedItemsDao(): OrderedItemsDao

    companion object {
        private const val DB_NAME = "assessment_app"

        @Volatile
        private var INSTANCE: AssessmentDb? = null

        fun getInstance(context: Context): AssessmentDb =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, AssessmentDb::class.java, DB_NAME).build()
    }
}