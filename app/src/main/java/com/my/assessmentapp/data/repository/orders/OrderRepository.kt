package com.my.assessmentapp.data.repository.orders

import com.my.assessmentapp.data.model.OrderedItems
import com.my.assessmentapp.data.model.Orders
import kotlinx.coroutines.flow.Flow

interface OrderRepository {

    fun createCartItem(itemId: Int,count: Int,itemName: String,price: Double,image: String,totalPrice: Double)

    fun getOrderedItems():Flow<List<OrderedItems>>

    fun deleteItemById(id: Int)

    fun deleteAll()

    fun update(id: Int,totalPrice: Double,count: Int)

    fun createOrder(totalPrice: Double)

    fun getOrders():Flow<List<Orders>>

}