package com.my.assessmentapp.data.repository.orders

import com.my.assessmentapp.data.local.dao.OrderDao
import com.my.assessmentapp.data.local.dao.OrderedItemsDao
import com.my.assessmentapp.data.model.OrderedItems
import com.my.assessmentapp.data.model.Orders
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.text.SimpleDateFormat
import java.util.*

class OrderRepositoryImpl(val orderedItemsDao: OrderedItemsDao,val orderDao: OrderDao):OrderRepository {
    override fun createCartItem(itemId: Int, count: Int,itemName: String,price: Double,image: String,totalPrice: Double) {
        orderedItemsDao.insert(
            OrderedItems(
                item_id = itemId,
                item_count = count,
                item_name = itemName,
                price = price,
                image = image,
                total_price = totalPrice
            )
        )
    }

    override fun getOrderedItems(): Flow<List<OrderedItems>> = flow {
        val result = orderedItemsDao.selectCartItems()
        emit(result)
    }.flowOn(Dispatchers.IO)

    override fun deleteItemById(id: Int) {
        orderedItemsDao.deleteItemById(id)
    }

    override fun deleteAll() {
        orderedItemsDao.deleteAll()
    }

    override fun update(id: Int, totalPrice: Double,count: Int) {
        orderedItemsDao.updateItemCount(id,count,totalPrice)
    }

    override fun createOrder(totalPrice: Double) {
        val c = Calendar.getInstance().time
        println("Current time => $c")

        val df = SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault())
        val formattedDate: String = df.format(c)
        orderDao.insert(Orders(total_price = totalPrice,date = formattedDate))
    }

    override fun getOrders(): Flow<List<Orders>> = flow {
        val result = orderDao.selectOrders()
        emit(result)
    }.flowOn(Dispatchers.IO)

}