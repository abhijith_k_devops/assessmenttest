package com.my.assessmentapp.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.my.assessmentapp.data.model.OrderedItems
import com.my.assessmentapp.data.model.Orders

@Dao
interface OrderDao: BaseDao<Orders> {

    @Query("SELECT * FROM ${Orders.TABLE_NAME}")
    fun selectOrders(): List<Orders>
}