package com.my.assessmentapp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.my.assessmentapp.data.model.Category
import com.my.assessmentapp.data.model.Items

@Dao
interface CategoryDao : BaseDao<Category>{
    @Query("SELECT * FROM ${Category.TABLE_NAME}")
    fun selectCategory(): List<Category>
}