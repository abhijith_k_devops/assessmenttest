package com.my.assessmentapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.my.assessmentapp.data.model.Orders.Companion.TABLE_NAME
import java.util.*

@Entity(tableName = TABLE_NAME)
data class Orders(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val total_price: Double,
    val date: String
){
    companion object {
        const val TABLE_NAME = "task_orders"
    }
}
