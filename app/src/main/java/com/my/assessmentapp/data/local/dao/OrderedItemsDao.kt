package com.my.assessmentapp.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.my.assessmentapp.data.model.Items
import com.my.assessmentapp.data.model.OrderedItems

@Dao
interface OrderedItemsDao: BaseDao<OrderedItems> {
    @Query("SELECT * FROM ${OrderedItems.TABLE_NAME}")
    fun selectCartItems(): List<OrderedItems>

    @Query("UPDATE ${OrderedItems.TABLE_NAME} SET item_count = :count,total_price = :totalPrice WHERE id = :id")
    fun updateItemCount(id: Int,count: Int,totalPrice: Double):Int

    @Query("DELETE FROM ${OrderedItems.TABLE_NAME}")
    fun deleteAll()

    @Query("DELETE FROM ${OrderedItems.TABLE_NAME} WHERE id = :id")
    fun deleteItemById(id: Int)
}