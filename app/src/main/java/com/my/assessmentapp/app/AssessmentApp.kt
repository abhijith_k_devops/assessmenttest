package com.my.assessmentapp.app

import android.app.Application
import android.util.Log
import androidx.databinding.library.BuildConfig
import com.my.assessmentapp.di.assessmentApp
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber


class AssessmentApp: Application() {
    override fun onCreate() {
        super.onCreate()

        /** start Koin context */
        startKoin {
            androidContext(this@AssessmentApp)
            // declare modules
            modules(assessmentApp)
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }

    }

    /** A tree which logs important information for crash reporting.  */
    private class CrashReportingTree : Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }

            //  FakeCrashLibrary.log(priority, tag, message)
            if (t != null) {
                if (priority == Log.ERROR) {
                    // FakeCrashLibrary.logError(t)
                } else if (priority == Log.WARN) {
                    // FakeCrashLibrary.logWarning(t)
                }
            }
        }
    }
}