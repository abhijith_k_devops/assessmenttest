package com.my.assessmentapp.ui.home.cart

import com.my.assessmentapp.data.model.OrderedItems

interface CartCallBack {
    fun onDeleteClick(orderedItems: OrderedItems)
}