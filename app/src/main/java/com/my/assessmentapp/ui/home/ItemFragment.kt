package com.my.assessmentapp.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.my.assessmentapp.AppExecutors
import com.my.assessmentapp.R
import com.my.assessmentapp.data.model.Items
import com.my.assessmentapp.databinding.FragmentItemBinding
import com.my.assessmentapp.ui.common.SpacingDecoration
import com.my.assessmentapp.ui.home.cart.ItemConfirmDialogFragment
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class ItemFragment : Fragment() {

    lateinit var binding: FragmentItemBinding
    private val appExecutors: AppExecutors by inject()
    val viewModel: HomeViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentItemBinding.inflate(inflater,container,false)
        viewModel.getItem(requireArguments().getInt(CATEGORY_ID))
        binding.lifecycleOwner = viewLifecycleOwner
        binding.itemRecyclerView.addItemDecoration(SpacingDecoration(requireContext(), R.dimen._4sdp))
        val column = resources.getInteger(R.integer.gallery_columns)
        binding.itemRecyclerView.layoutManager = GridLayoutManager(requireContext(),column)
        val adapter = ItemAdapter(
            appExecutors,
            requireContext(),
            object : ItemCallBack {
                override fun onClick(item: Items) {
                    val fragment = ItemConfirmDialogFragment(item)
                    fragment.show(childFragmentManager,"item-confirm")
                }

            }
        )
        binding.itemRecyclerView.adapter = adapter
        viewModel.itemDataState.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()){
                adapter.submitList(emptyList())
            }else{
                adapter.submitList(it)
            }
        })
        return binding.root
    }
    companion object {
        const val CATEGORY_ID = "id"
    }
}