package com.my.assessmentapp.ui.home

import com.my.assessmentapp.data.model.Items

interface ItemCallBack {
    fun onClick(item: Items)
}