package com.my.assessmentapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.my.assessmentapp.R
import com.my.assessmentapp.databinding.ActivityMainBinding
import com.my.assessmentapp.ui.home.HomeFragment
import com.my.assessmentapp.ui.transaction.TransactionFragment

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val fragment = HomeFragment()
        replaceFragment(fragment)
        initialiseUi()
        setContentView(binding.root)

    }
    private fun initialiseUi()
    {
        binding.bottomNavigationView.setOnNavigationItemSelectedListener { menuItem ->
            when(menuItem.itemId){
                R.id.home -> {
                        val fragment = HomeFragment()
                        replaceFragment(fragment)
                        return@setOnNavigationItemSelectedListener true
                }
                R.id.transaction -> {
                    val fragment = TransactionFragment()
                    replaceFragment(fragment)
                    return@setOnNavigationItemSelectedListener true
                }

            }
            false
        }
    }
    private fun replaceFragment(fragment: Fragment)
    {
        supportFragmentManager.beginTransaction().replace(
            R.id.home_container,
            fragment,
            fragment.javaClass.getSimpleName()
        )
            .commit()
    }
}