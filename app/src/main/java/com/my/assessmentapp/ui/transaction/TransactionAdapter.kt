package com.my.assessmentapp.ui.transaction

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.my.assessmentapp.AppExecutors
import com.my.assessmentapp.R
import com.my.assessmentapp.data.model.Orders
import com.my.assessmentapp.databinding.LayoutTransactionItemBinding
import com.my.assessmentapp.ui.common.DataBoundListAdapter

class TransactionAdapter(appExecutors: AppExecutors): DataBoundListAdapter<Orders,LayoutTransactionItemBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Orders>(){
        override fun areItemsTheSame(oldItem: Orders, newItem: Orders): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Orders, newItem: Orders): Boolean {
            return oldItem.id == newItem.id
        }

    }
) {
    override fun createBinding(parent: ViewGroup): LayoutTransactionItemBinding {

        return DataBindingUtil
            .inflate(
                LayoutInflater.from(parent.context),
                R.layout.layout_transaction_item,
                parent,
                false
            )
    }

    override fun bind(binding: LayoutTransactionItemBinding, item: Orders) {
        binding.price = String.format("%.2f",item.total_price)+" $"
        binding.date = item.date
    }
}