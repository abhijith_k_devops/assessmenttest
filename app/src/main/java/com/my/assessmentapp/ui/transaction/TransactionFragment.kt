package com.my.assessmentapp.ui.transaction

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.my.assessmentapp.AppExecutors
import com.my.assessmentapp.R
import com.my.assessmentapp.databinding.FragmentTransactionBinding
import com.my.assessmentapp.databinding.LayoutCartItemBinding
import com.my.assessmentapp.ui.common.SpacingDecoration
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class TransactionFragment : Fragment() {


    lateinit var binding: FragmentTransactionBinding
    val viewModel:TransactionViewModel by viewModel()
    val appExecutors: AppExecutors by inject()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTransactionBinding.inflate(inflater,container,false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.transactionRecyclerView.addItemDecoration(SpacingDecoration(requireContext(), R.dimen._4sdp))
        val column = resources.getInteger(R.integer.gallery_columns)
        binding.transactionRecyclerView.layoutManager = GridLayoutManager(requireContext(),column)
        val adapter = TransactionAdapter(appExecutors)
        binding.transactionRecyclerView.adapter = adapter
        viewModel.orderItemDataState.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()){
                adapter.submitList(emptyList())
            }else{
                adapter.submitList(it)
            }
        })
        return binding.root
    }


}