package com.my.assessmentapp.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.my.assessmentapp.data.model.Category
import com.my.assessmentapp.data.repository.stock.StockRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

class SplashViewModel(val stockRepository: StockRepository) : ViewModel() {

    private var _tokenDataState : MutableLiveData<List<Category>> = MutableLiveData()
    val tokenDataState : LiveData<List<Category>> = _tokenDataState

    init {
        getCategory()
    }
    fun createStock(){
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            stockRepository.createStock()
        }
    }
    fun getCategory(){
        viewModelScope.launch {
            stockRepository.selectCategory()
                .collect { _tokenDataState.value = it }
        }
    }
}