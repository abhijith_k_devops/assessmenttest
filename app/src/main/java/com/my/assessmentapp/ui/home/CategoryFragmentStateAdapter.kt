package com.my.assessmentapp.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.my.assessmentapp.data.model.Category

class CategoryFragmentStateAdapter(fragment: Fragment?,val list: ArrayList<Category>): FragmentStateAdapter(fragment!!){
    override fun getItemCount(): Int {
        return list.size
    }

    override fun createFragment(position: Int): Fragment {
        val fragment: Fragment = ItemFragment()
        val args = Bundle()
        val categoryItem = list[position]
        // Our object is just an integer :-P
        val id = categoryItem.id
        args.putInt(ItemFragment.CATEGORY_ID, id)
        fragment.arguments = args
        return fragment
    }

}