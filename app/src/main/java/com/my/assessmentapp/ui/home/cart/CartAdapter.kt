package com.my.assessmentapp.ui.home.cart

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.my.assessmentapp.AppExecutors
import com.my.assessmentapp.R
import com.my.assessmentapp.data.model.OrderedItems
import com.my.assessmentapp.databinding.LayoutCartItemBinding
import com.my.assessmentapp.databinding.LayoutItemListBinding
import com.my.assessmentapp.ui.common.DataBoundListAdapter

class CartAdapter(
    appExecutors: AppExecutors,
    val context: Context,
    val viewModel: CartViewModel,
    val cartCallBack: CartCallBack

):DataBoundListAdapter<OrderedItems,LayoutCartItemBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<OrderedItems>(){
        override fun areItemsTheSame(oldItem: OrderedItems, newItem: OrderedItems): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: OrderedItems, newItem: OrderedItems): Boolean {
            return oldItem.id == newItem.id
        }
    }
) {
    override fun createBinding(parent: ViewGroup): LayoutCartItemBinding {
        val binding =  DataBindingUtil
            .inflate<LayoutCartItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.layout_cart_item,
                parent,
                false
            )
        binding.ivBtnDelete.setOnClickListener {
            binding.item.let {
                cartCallBack.onDeleteClick(it!!)
            }
        }

        return binding
    }

    override fun bind(binding: LayoutCartItemBinding, item: OrderedItems) {
        binding.item = item
        binding.count = item.item_count.toString()
        binding.price = "${item.price} $"
        val taxAmount = (item.price * 12.05)/100
        val totalPrice = String.format("%.2f", item.price + taxAmount).toDouble()
        val resourceImage: Int = context.getResources()
            .getIdentifier(item.image, "drawable", context.packageName)
        binding.itemImage.setImageDrawable(context.resources.getDrawable(resourceImage))
        var count = item.item_count
        binding.ivMinus.setOnClickListener {
            if(count>1){
                count--
                viewModel.updateById(item.id,count,item.total_price - totalPrice)
                viewModel.getCartItems()
                notifyDataSetChanged()
                binding.count = count.toString()
            }
        }
        binding.ivPlus.setOnClickListener {
            count++
            viewModel.updateById(item.id,count,item.total_price+totalPrice)
            viewModel.getCartItems()
            notifyDataSetChanged()
            binding.count = count.toString()
        }
    }
}