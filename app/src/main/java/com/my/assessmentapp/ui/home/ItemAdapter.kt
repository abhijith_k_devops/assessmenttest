package com.my.assessmentapp.ui.home

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.my.assessmentapp.AppExecutors
import com.my.assessmentapp.R
import com.my.assessmentapp.data.model.Category
import com.my.assessmentapp.data.model.Items
import com.my.assessmentapp.databinding.LayoutItemListBinding
import com.my.assessmentapp.ui.common.DataBoundListAdapter

class ItemAdapter(
    appExecutors: AppExecutors,
    val context: Context,
    val itemCallBack: ItemCallBack
): DataBoundListAdapter<Items,LayoutItemListBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Items>(){
        override fun areItemsTheSame(oldItem: Items, newItem: Items): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Items, newItem: Items): Boolean {
            return oldItem.id == newItem.id
        }

    }
) {
    override fun createBinding(parent: ViewGroup): LayoutItemListBinding {
        val binding =  DataBindingUtil
            .inflate<LayoutItemListBinding>(
                LayoutInflater.from(parent.context),
                R.layout.layout_item_list,
                parent,
                false
            )
        binding.root.setOnClickListener {
            binding.item.let {
                itemCallBack.onClick(it!!)
            }
        }
        return binding
    }

    override fun bind(binding: LayoutItemListBinding, item: Items) {
        binding.item = item
        binding.price = "${item.price} $"
        val resourceImage: Int = context.getResources()
            .getIdentifier(item.item_image, "drawable", context.packageName)
        binding.ivPoster.setImageDrawable(context.resources.getDrawable(resourceImage))

    }
}