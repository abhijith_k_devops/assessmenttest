package com.my.assessmentapp.ui.home.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.my.assessmentapp.data.model.OrderedItems
import com.my.assessmentapp.data.repository.orders.OrderRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class CartViewModel(val orderRepository: OrderRepository) : ViewModel() {

    private var _orderItemDataState : MutableLiveData<List<OrderedItems>> = MutableLiveData()
    val orderItemDataState : LiveData<List<OrderedItems>> = _orderItemDataState

    init {
        getCartItems()
    }
    fun createCartItem(
        itemId: Int,
        count: Int,
        itemName: String,
        price: Double,
        image: String,
        totalPrice: Double
    ){
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            orderRepository.createCartItem(itemId, count,itemName,price,image,totalPrice)
        }
    }

    fun deleteAll(){
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            orderRepository.deleteAll()
        }
    }
    fun deleteById(id: Int){
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            orderRepository.deleteItemById(id)
        }
    }

    fun updateById(id: Int,count: Int,totalPrice: Double){
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            orderRepository.update(id,totalPrice,count)
        }
    }

    fun getCartItems(){
        viewModelScope.launch {
            orderRepository.getOrderedItems()
                .collect { _orderItemDataState.value = it }
        }
    }
    fun createOrder(totalPrice: Double){
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            orderRepository.createOrder(totalPrice)
        }
    }

}