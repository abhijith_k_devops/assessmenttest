package com.my.assessmentapp.ui.home.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.my.assessmentapp.data.model.Items
import com.my.assessmentapp.databinding.FragmentItemConfirmDialogBinding
import com.my.assessmentapp.ui.custom.RoundedBottomSheetDialogFragment
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class ItemConfirmDialogFragment(val item: Items) : RoundedBottomSheetDialogFragment() {

    lateinit var binding: FragmentItemConfirmDialogBinding
    val viewModel: CartViewModel by inject()
    var count = 1
    var totalPrice: Double = 1.0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        binding= FragmentItemConfirmDialogBinding.inflate(inflater,container,false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.item = item
        binding.subTotal = "${item.price * count} $"
        binding.price = "${item.price} $"
        val taxAmount = (item.price * 12.05)/100
        totalPrice = String.format("%.2f", item.price + taxAmount).toDouble()
        binding.total = "${String.format("%.2f", item.price + taxAmount)} $"
        binding.count = "1"
        val resourceImage: Int = requireContext().getResources()
            .getIdentifier(item.item_image, "drawable", requireContext().packageName)
        binding.itemImage.setImageDrawable(requireContext().resources.getDrawable(resourceImage))

        binding.buttonConfirm.setOnClickListener {

            viewModel.createCartItem(item.id,count,item.item,item.price,item.item_image,totalPrice)
            dismiss()
        }

        binding.ivPlus.setOnClickListener {
            count++
            binding.count = count.toString()
            var price = item.price * count
            val taxAmount = (price * 12.05)/100
            totalPrice = price + taxAmount
            binding.total = "${String.format("%.2f", totalPrice)} $"
            binding.subTotal = "${String.format("%.2f",price)} $"
        }
        binding.ivMinus.setOnClickListener {
            if (count > 1){
                count--
            }
            binding.count = count.toString()
            var price = item.price * count
            val taxAmount = (price * 12.05)/100
            totalPrice = price + taxAmount
            binding.total = "${String.format("%.2f", totalPrice)} $"
            binding.subTotal = "${String.format("%.2f",price)} $"
        }
        return binding.root
    }

}