package com.my.assessmentapp.ui.home.cart

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.my.assessmentapp.AppExecutors
import com.my.assessmentapp.R
import com.my.assessmentapp.data.model.OrderedItems
import com.my.assessmentapp.databinding.FragmentCartBinding
import com.my.assessmentapp.ui.common.SpacingDecoration
import org.jetbrains.anko.support.v4.toast
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class CartFragment : DialogFragment() {

    lateinit var binding: FragmentCartBinding
    val viewModel: CartViewModel by viewModel()
    private val appExecutors: AppExecutors by inject()
    var total = 1.0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        binding = FragmentCartBinding.inflate(inflater,container,false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.cartRecyclerView.addItemDecoration(SpacingDecoration(requireContext(), R.dimen._4sdp))
        binding.cartRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        val adapter = CartAdapter(
            appExecutors,
            requireContext(),
            viewModel,
            object : CartCallBack {
                override fun onDeleteClick(orderedItems: OrderedItems) {
                    viewModel.deleteById(orderedItems.id)
                    viewModel.getCartItems()
                }
            }
        )
        binding.dismiss.setOnClickListener {
            dismiss()
        }
        binding.delete.setOnClickListener {
            viewModel.deleteAll()
            viewModel.getCartItems()
        }
        binding.buttonCheckout.setOnClickListener {
            viewModel.createOrder(total)
            viewModel.deleteAll()
            toast("Order placed")
            dismiss()
        }
        binding.cartRecyclerView.adapter = adapter
        viewModel.orderItemDataState.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) {
                adapter.submitList(emptyList())
                binding.delete.visibility = View.GONE
                binding.linearFooter.visibility = View.INVISIBLE
                binding.splitter.visibility = View.INVISIBLE
            } else {
                adapter.submitList(it)
                total = it.map { it.total_price }.sum()
                val sum = String.format("%.2f",total)
                binding.total = "$sum $"
            }
        })
        // Inflate the layout for this fragment
        return binding.root
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme)
    }

}