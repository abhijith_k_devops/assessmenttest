package com.my.assessmentapp.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayoutMediator
import com.my.assessmentapp.R
import com.my.assessmentapp.data.model.Category
import com.my.assessmentapp.databinding.FragmentHomeBinding
import com.my.assessmentapp.utils.autoCleared
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.support.v4.toast
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.my.assessmentapp.ui.home.cart.CartFragment


class HomeFragment : Fragment() {

    lateinit var binding: FragmentHomeBinding
    val viewModel: HomeViewModel by viewModel()
    private var categoryFragmentStateAdapter by autoCleared<CategoryFragmentStateAdapter>()
    @SuppressLint("UnsafeExperimentalUsageError")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater,container,false)
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.categoryDataState.observe(viewLifecycleOwner, Observer { category ->
            categoryFragmentStateAdapter = CategoryFragmentStateAdapter(this,category as ArrayList<Category>)
            binding.homeViewPager.adapter = categoryFragmentStateAdapter
            TabLayoutMediator(categoryTab,homeViewPager){tab,position->
                tab.text = category[position].category
            }.attach()
        })
        binding.ivBtnBag.setOnClickListener {
            CartFragment().show(childFragmentManager,"cart-dialog")
        }
        val badgeDrawable = BadgeDrawable.create(requireContext())
        badgeDrawable.isVisible = true
        badgeDrawable.backgroundColor = resources.getColor(R.color.red)
        badgeDrawable.badgeGravity = BadgeDrawable.TOP_END
        BadgeUtils.attachBadgeDrawable(badgeDrawable, binding.ivBtnBag);
        return binding.root
    }


}