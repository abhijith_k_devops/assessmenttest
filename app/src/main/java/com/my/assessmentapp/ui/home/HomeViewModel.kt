package com.my.assessmentapp.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.my.assessmentapp.data.model.Category
import com.my.assessmentapp.data.model.Items
import com.my.assessmentapp.data.repository.stock.StockRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeViewModel(val stockRepository: StockRepository): ViewModel() {
    private var _categoryDataState : MutableLiveData<List<Category>> = MutableLiveData()
    val categoryDataState : LiveData<List<Category>> = _categoryDataState

    private var _itemDataState : MutableLiveData<List<Items>> = MutableLiveData()
    val itemDataState : LiveData<List<Items>> = _itemDataState

    init {
        getCategory()
    }
    fun getCategory(){
        viewModelScope.launch {
            stockRepository.selectCategory()
                .collect { _categoryDataState.value = it }
        }
    }
    fun getItem(id : Int){
        viewModelScope.launch {
            stockRepository.selectItem(id)
                .collect { _itemDataState.value = it }
        }
    }
}