package com.my.assessmentapp.ui.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.my.assessmentapp.data.model.OrderedItems
import com.my.assessmentapp.data.model.Orders
import com.my.assessmentapp.data.repository.orders.OrderRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class TransactionViewModel(val orderRepository: OrderRepository) : ViewModel() {
    private var _orderItemDataState : MutableLiveData<List<Orders>> = MutableLiveData()
    val orderItemDataState : LiveData<List<Orders>> = _orderItemDataState
    init {
        getOrders()
    }
    fun getOrders(){
        viewModelScope.launch {
            orderRepository.getOrders()
                .collect { _orderItemDataState.value = it }
        }
    }

}