package com.my.assessmentapp.di

import com.my.assessmentapp.data.local.db.AssessmentDb
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {

    single {
        AssessmentDb.getInstance(androidApplication())
    }
    single { get<AssessmentDb>().categoryDao() }
    single { get<AssessmentDb>().orderDao() }
    single { get<AssessmentDb>().itemDao() }
    single { get<AssessmentDb>().orderedItemsDao() }
}