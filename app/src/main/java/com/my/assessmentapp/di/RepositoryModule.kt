package com.my.assessmentapp.di


import com.my.assessmentapp.data.repository.orders.OrderRepository
import com.my.assessmentapp.data.repository.orders.OrderRepositoryImpl
import com.my.assessmentapp.data.repository.stock.StockRepository
import com.my.assessmentapp.data.repository.stock.StockRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single { StockRepositoryImpl(get(),get()) as StockRepository }
    single { OrderRepositoryImpl(get(),get()) as OrderRepository }
}
