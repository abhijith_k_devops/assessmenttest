package com.my.assessmentapp.di


import com.my.assessmentapp.ui.home.HomeViewModel
import com.my.assessmentapp.ui.home.cart.CartViewModel
import com.my.assessmentapp.ui.splash.SplashViewModel
import com.my.assessmentapp.ui.transaction.TransactionViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { SplashViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { CartViewModel(get()) }
    viewModel { TransactionViewModel(get()) }
}
