package com.my.assessmentapp.di

import com.my.assessmentapp.AppExecutors
import org.koin.dsl.module

val appExecutorsModule = module {
    single { AppExecutors() }
}