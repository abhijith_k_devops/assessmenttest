package com.my.assessmentapp.di

val assessmentApp = listOf(
    databaseModule,
    repositoryModule,
    viewModelModule,
    appExecutorsModule
)